//
//  SplitViewController.swift
//  SplitViewTesting
//
//  Created by Antonín Charvát on 25/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class SplitViewController: UISplitViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.preferredDisplayMode = .Automatic

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
