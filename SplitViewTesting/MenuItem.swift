//
//  MenuItem.swift
//  SplitViewTesting
//
//  Created by Antonín Charvát on 25/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

enum Item: Int {
    case Inbox
    case Starred
    case Drafts
    case Groups
    case SharedTemplates
    case Calendar
    case Todos
    case Group
}

class MenuItem {
    let item: Item?
    let groupName: String?
    
    init(item: Item, withGroupName groupName: String?) {
        self.item = item
        self.groupName = groupName
    }
    
    func itemImage() -> UIImage? {
        if self.item != .Group {
            return MenuItems.itemsIcons[self.item!.rawValue]
        }
        return nil
    }
    
    func itemName() -> String {
        switch (self.item!) {
        case .Inbox:
            return MenuItems.items[0]
        case .Starred:
            return MenuItems.items[1]
        case .Drafts:
            return MenuItems.items[2]
        case .Groups:
            return MenuItems.items[3]
        case .SharedTemplates:
            return MenuItems.items[4]
        case .Calendar:
            return MenuItems.items[5]
        case .Todos:
            return MenuItems.items[6]
        case .Group:
            return self.groupName!
        }
    }
    
}