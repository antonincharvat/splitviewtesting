//
//  MenuHeaderCell.swift
//  JonkyKongTesting
//
//  Created by Antonín Charvát on 24/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class MenuHeaderCell: UITableViewCell {
    
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var headerName: UILabel!
    @IBOutlet weak var headerPosition: UILabel!
    
    var tapRecognizer = UITapGestureRecognizer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addGestureRecognizer(tapRecognizer)
        self.backgroundColor = UIColor.grayColor()
    }
    

    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
