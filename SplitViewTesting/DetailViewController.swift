//
//  DetailViewController.swift
//  SplitViewTesting
//
//  Created by Antonín Charvát on 25/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UIPopoverPresentationControllerDelegate {

    // MARK: Properties
    @IBOutlet weak var label: UILabel!
    var item: MenuItem? {
        didSet {
            setUp()
        }
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Setup
    func setUp() {
        if let item: MenuItem = self.item {
            if item.item == .Group {
                if let label = self.label {
                    label.text = item.groupName
                }
            }else{
                if let label = self.label {
                    label.text = item.itemName()
                }
            }
        }else{
            label.text = ""
        }
    }

    // MARK: Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "popoverSegue" {
            let pvc = segue.destinationViewController
            pvc.modalPresentationStyle = .Popover
            pvc.popoverPresentationController?.delegate = self
        }
    }
    
    // MARK: Popover delegate
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
}
