//
//  MenuGroupCell.swift
//  JonkyKongTesting
//
//  Created by Antonín Charvát on 24/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class MenuGroupCell: UITableViewCell {
    
    @IBOutlet weak var groupName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
