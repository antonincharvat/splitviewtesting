//
//  MasterViewController.swift
//  SplitViewTesting
//
//  Created by Antonín Charvát on 24/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController, UIPopoverPresentationControllerDelegate {

    // MARK: Properties
    var menuItems: [MenuItem] = []
    var groupsOpen = false {
        didSet {
            if groupsOpen == false {
                menuItems = loadMenuItems()
            }else{
                let groups = loadGroups()
                menuItems.insertContentsOf(groups, at: 4)
            }
        }
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // initial menu items load
        menuItems = loadMenuItems()
        
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            print(controllers)
        }
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: Helpers
    func loadMenuItems() -> [MenuItem] {
        var items: [MenuItem] = []
        let inbox = MenuItem(item: .Inbox, withGroupName: nil)
        let starred = MenuItem(item: .Starred, withGroupName: nil)
        let drafts = MenuItem(item: .Drafts, withGroupName: nil)
        let groups = MenuItem(item: .Groups, withGroupName: nil)
        let sharedTemp = MenuItem(item: .SharedTemplates, withGroupName: nil)
        let calendar = MenuItem(item: .Calendar, withGroupName: nil)
        let todos = MenuItem(item: .Todos, withGroupName: nil)
        items.appendContentsOf([inbox, starred, drafts, groups, sharedTemp, calendar, todos])
        
        return items
    }
    
    func loadGroups() -> [MenuItem] {
        var groups: [MenuItem] = []
        for item in MenuItems.groups {
            let group = MenuItem(item: .Group, withGroupName: item)
            groups.append(group)
        }
        return groups
    }
    
    
    // MARK: Table view data source
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCellWithIdentifier("MenuHeaderCell") as? MenuHeaderCell
        cell?.headerImage.image = MenuHeader.image
        cell?.headerName.text = MenuHeader.name
        cell?.headerPosition.text = MenuHeader.position
        cell?.tapRecognizer.addTarget(self, action: #selector(MasterViewController.headerCellTapped(_:)))
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 150
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if (indexPath.row > 3) && (indexPath.row < MenuItems.groups.count + 4) {
            if groupsOpen {
                let cell = tableView.dequeueReusableCellWithIdentifier("MenuGroupCell") as? MenuGroupCell
                cell?.groupName.text = menuItems[indexPath.row].groupName
                return cell!
            }
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("MenuItemCell") as? MenuItemCell
        var imageIndex: Int {
            if groupsOpen {
                if indexPath.row > MenuItems.groups.count + 3 {
                    // when groups are open, we need to offset the indexPath of image
                    return (indexPath.row - MenuItems.groups.count)
                }
            }
            return indexPath.row
        }
        cell?.itemImage.image = MenuItems.itemsIcons[imageIndex]
        cell?.itemName.text = menuItems[indexPath.row].itemName()
        
        if indexPath.row != 3 {
            cell?.accessoryType = .DisclosureIndicator
        }
        return cell!
    }

    
    // MARK: Table view delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 3 {
            groupsOpen = !groupsOpen
            tableView.reloadData()
        }else{
            self.performSegueWithIdentifier("showItemDetail", sender: self)
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
    func headerCellTapped(recognizer: UITapGestureRecognizer) {
        self.performSegueWithIdentifier("showHeaderDetail", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showItemDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let vc = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
                vc.item = menuItems[indexPath.row]
                vc.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                vc.navigationItem.leftItemsSupplementBackButton = true
            }
        }else if segue.identifier == "showHeaderDetail" {
            let vc = (segue.destinationViewController as! UINavigationController).topViewController as! ProfileViewController
            vc.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
            vc.navigationItem.leftItemsSupplementBackButton = true
        }else if segue.identifier == "popoverSegueMaster" {
            let pvc = segue.destinationViewController
            pvc.modalPresentationStyle = .Popover
            pvc.popoverPresentationController?.delegate = self
        }
    }
    
    
    // MARK: Popover delegate
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    
}
