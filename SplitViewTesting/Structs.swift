//
//  Structs.swift
//  JonkyKongTesting
//
//  Created by Antonín Charvát on 24/05/16.
//  Copyright © 2016 Antonín Charvát. All rights reserved.
//

import UIKit

struct MenuHeader {
    static let image = UIImage(named: "header_photo")
    static let name = "Jozef Ševčík"
    static let position = "Developer"
}

struct MenuItems {
    static let items = ["Inbox", "Starred", "Drafts (5)", "Groups", "Shared templates", "Calendar", "TODOs"]
    static let itemsIcons = [UIImage(named: "inbox_icon")!, UIImage(named: "starred_icon")!, UIImage(named: "drafts_icon")!, UIImage(named: "groups_icon")!, UIImage(named: "shared_templates_icon")!, UIImage(named: "calendar_icon")!, UIImage(named: "todos_icon")!]
    
    static let groups = ["Group1", "Group2", "Group3", "Group4", "Group5", "Group6", "Group7", "Group8", "Group9", "Group10", "Group11", "Group12", "Group13", "Group14", "Group15"]
}